from solcx import compile_standard
import json 
from web3 import Web3 
import os 
from dotenv import load_dotenv


load_dotenv()
with open('./SimpleStorage.sol','r') as file:
    simple_storage_file = file.read()

# print(simple_storage_file)
# Compile Our Solidity
compile_sol = compile_standard({
    "language":"Solidity",
    "sources": {
        "SimpleStorage.sol": {
            "content":simple_storage_file
        }
    },
    "settings": {
        "outputSelection": {
            "*": {
                "*":["abi", "metadata","evm.bytecode", "evm.sourceMap"]
            }
        }
    }  
},
)
# print(26,compile_sol)
with open("compile_code.json", "w") as file:
    json.dump(compile_sol, file)

contract_id, contract_interface = compile_sol.popitem()
# print(contract_interface)
# bytecode = contract_interface['bin']
# abi = contract_interface['abi']
# get bytecode
byte_code = compile_sol["contracts"]["SimpleStorage.sol"]["SimpleStorage"]["evm"]["bytecode"]["object"]
abi = compile_sol["contracts"]["SimpleStorage.sol"]["SimpleStorage"]["abi"]
# print(abi)
# print(byte_code)

#  Connecting to ganache  
w3 = Web3(Web3.HTTPProvider("https://rinkeby.infura.io/v3/21376fecd52b4e73814b72c39fe1707b"))

chain_id = 4
my_address = "0x28fECA01A8792C4Df3D5f8e9571F91dC7CeE3354"
# private_key = "25e1ce114ed2ce58175fe8602fb0e4a67909d66646583b781f5c04cd79813a97"
private_key = os.getenv("PRIVATE_KEY")
# create contract in python 
SimpleStorage = w3.eth.contract(abi=abi,bytecode=byte_code)
# SimpleStorage =w3.eth.contract(address=my_address)
# get latest transaction 
nonce = w3.eth.getTransactionCount(my_address)
print(nonce)
#  1. Build a transaction
#  2. Sign a transaction
#  3. Send a transaction


transaction = SimpleStorage.constructor().buildTransaction({
    "gasPrice": w3.eth.gas_price, 
    "chainId":chain_id,
    "from":my_address,"nonce":nonce
})
# print(transaction)  

signed_trx = w3.eth.account.sign_transaction(transaction, private_key=private_key)
# print(signed_trx)
print("Deploying contract")
# SEND transaction
tx_hash = w3.eth.send_raw_transaction(signed_trx.rawTransaction)
tx_receipt = w3.eth.wait_for_transaction_receipt(tx_hash)
print("Deployed")
# Working with contract

# contrac abi , address
# initial value of favorite number 
simple_storage = w3.eth.contract(address = tx_receipt.contractAddress, abi=abi)
print(simple_storage.functions.retrieve().call())
print("Updating contract ...")
# print(simple_storage.functions.store(15).call())
# print(simple_storage.functions.retrieve().call())

store_transaction = simple_storage.functions.store(15).buildTransaction({
    "gasPrice": w3.eth.gas_price, 
    "chainId":chain_id,
    "from":my_address,
    "nonce":nonce+1
})
signed_store_tx = w3.eth.account.sign_transaction(store_transaction, private_key=private_key)
send_store_tx = w3.eth.send_raw_transaction(signed_store_tx.rawTransaction)
tx_receipt = w3.eth.wait_for_transaction_receipt(send_store_tx)
print("Updated ")
print(simple_storage.functions.retrieve().call())
