from brownie import accounts, config, SimpleStorage, network
import os 

def deploy_simple_storage():
    # account = accounts[0]
    account = get_account()
    simple_storage = SimpleStorage.deploy({"from":account})
    stored_value = simple_storage.retrieve()
    print(stored_value)
    transaction = simple_storage.store(15, {"from":account})
    transaction.wait(1)
    updated_store_value = simple_storage.retrieve()
    print(12, updated_store_value)
    # account = accounts.load("test")
    # account =  accounts.add(os.getenv("PRIVATE_KEY"))
    # account = accounts.add(config['wallets']['from_key'])
    # print(simple_storage)

def get_account():
    if network.show_active() == "development":
        return accounts[0]
    else:
        return accounts.add(config['wallets']['from_key'])

def main():
    print("Hello")
    deploy_simple_storage()