from brownie import SimpleStorage, accounts

def test_deploy():
    # arrange
    account = accounts[0]
    simple_storage = SimpleStorage.deploy({"from":account})
    starting_value = simple_storage.retrieve()
    expected = 0
    assert starting_value == expected
    # act
    # assert

def test_updating_storage():
    # arrange
    # act
    # assert
    account = accounts[0]
    simple_storage = SimpleStorage.deploy({"from":account})
    expected = 15
    simple_storage.store(expected, {"from":account})
    assert expected == simple_storage.retrieve()