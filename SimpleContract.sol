// SPDX-License-Identifier: MIT
pragma solidity ^0.6.0;

contract SimpleContract {
  uint data;
  function updateData(uint _data) external {
    data = _data;
  }
  function readData() external view returns(uint) {
    return data;
  }
}
